var gulp = require('gulp');
var sourceMaps = require('gulp-sourcemaps');
var sass = require('gulp-sass');
var condition = require('gulp-if');
var concat = require('gulp-concat');

var createSASSPreprocessor = function(args, config) {
    config = config || {};
    return function(content, file, done) {
        gulp.src(config.source).
            pipe(condition(config.createSourceMaps, sourceMaps.init())).
            pipe(sass({
                onError: function(err) {
                    done(err);
                }
            })).
            pipe(condition(config.createSourceMaps, sourceMaps.write())).
            pipe(concat(config.outputFile)).
            pipe(gulp.dest(config.outputDir)).
            on('end', function() {
                done(null, content);
            });
    };
};

createSASSPreprocessor.$inject = ['args', 'config.sassPreprocessor'];

// PUBLISH DI MODULE
module.exports = {
    'preprocessor:sass': ['factory', createSASSPreprocessor]
};