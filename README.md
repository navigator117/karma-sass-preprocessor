# karma-sass-preprocessor

> Preprocessor to watch sass files changes and build css on the fly.

## Installation

The easiest way is to keep `karma-sass-preprocessor` as a devDependency in your `package.json`.
```json
{
  "devDependencies": {
    "karma": "~0.10",
    "karma-sass-preprocessor": "~0.0.1"
  }
}
```

You can simple do it by:
```bash
npm install karma-sass-preprocessor --save-dev
```

## Configuration
Following code shows the example configuration...
```js
// karma.conf.js
module.exports = function(config) {
  config.set({
    files: [
      {
        pattern: 'scss/**/*.scss',
        watched: true,
        included: false,
        served: true
      },
      'test/unit-sass.css'
    ],
    preprocessors: {
      'scss/**/*.scss': ['sass']
    },
    sassPreprocessor: {
      source: 'scss/*.scss',
      createSourceMaps: true,
      outputDir:  __dirname + '/test/',
      outputFile: 'unit-sass.css'
    }    
  });
};
```

----

For more information on Karma see the [homepage].


[homepage]: http://karma-runner.github.com